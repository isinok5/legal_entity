import React from 'react';
import Header from './components/Header';
import Login from './components/Login';
import LegalEntities from './components/LegalEntities';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Header/>
        <Route path='/login' exact render={() => (
          LegalEntities.isLoggedIn() ? (
            <Redirect to='/'/> ) : (
             <Login/>)
        )}/>
        <Route path='/entities' exact render={() => (
          LegalEntities.isLoggedIn() ? (
            <LegalEntities/> ) : (
            <Redirect to='/login'/>)
        )}/>
      </div>
    </Router>
  );
}

export default App;
