import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Header extends Component {
    state = {
        isLoggedIn: false,
    }

    isLoggedIn = () => {
        const admin = window.localStorage.getItem('admin');
        const user = window.localStorage.getItem('user');
        if (admin === 'admin777' || user === 'user777')
            return this.setState({ isLoggedIn: !this.state.isLoggedIn});
    }

    componentDidMount() {
        this.isLoggedIn();
    }

    exitFromApp() {
        if (localStorage.getItem('admin'))
                delete localStorage['admin']
        if (localStorage.getItem('user'))
                delete localStorage['user']
            this.setState({ isLoggedIn: !this.state.isLoggedIn});
    }

    render() {
        return(
            <header>
                <div className="container h-flex">
                    <Link to='/' className='menu__links'>
                         <h1>Legal Entity</h1>
                    </Link>
                    <nav className="links">
                        <ul>
                            {this.state.isLoggedIn ?
                                null :
                                (
                                    <li>
                                        <Link to='/login' className='menu__links'>Войти</Link>
                                    </li> 
                                )
                            }
                            {this.state.isLoggedIn ?
                                <li>
                                    <Link to='/entities' className='menu__links'>Список Юр. Лиц</Link>
                                </li> :
                                null
                            }
                            {this.state.isLoggedIn ?
                                <li>
                                    <Link to='/' className='menu__links' onClick={() => this.exitFromApp()}>Выход</Link>
                                </li>:
                                null
                            }
                        </ul>
                    </nav>
                </div>
            </header>
        )
    }
}
