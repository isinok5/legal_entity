import React, {Component} from 'react';
import LegalEntities from './LegalEntities';

export default class Entity extends Component {
    state = {
        showEntities: false,
        showOneEntity: true,
    }

    backToEntities = () => {
        this.setState({
            showEntities: !this.state.showEntities,
            showOneEntity: !this.state.showOneEntity,
        })
    }

    render() {
        const {name, inn, address, city, phone, director} = this.props;
        return (
            <div>
                { this.state.showOneEntity ?
                    <div>
                        <div className="entity">
                            <li>
                                <p>Название организации</p>
                                <div>{name}</div>
                            </li>
                            <li>
                                <p>ИНН</p>
                                <div>{inn}</div>
                            </li>
                            <li>
                                <p>Адрес</p>
                                <div>{address}</div>
                            </li>
                            <li>
                                <p>Город</p>
                                <div>{city}</div>
                            </li>
                            <li>
                                <p>Телефон</p>
                                <div>{phone}</div>
                            </li>
                            <li>
                                <p>Директор</p>
                                <div>{director}</div>
                            </li>
                        </div> 
                        <button className="t-button" onClick={() => this.backToEntities()}>Назад</button>  
                    </div>:
                    null
                }
                {this.state.showEntities ? <LegalEntities/> : null}
            </div>
        )
    }
}
