import React, {Component} from 'react';

export default class Login extends Component {
    state = {
        isUser: [
            {
                name: "admin",
                password: "admin777",
            },
            {
                name: "user",
                password: "user777",
            }
        ],
        login: '',
        password: '',
        loginValid: false,
        passwordValid: false,
        
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const login = e.target.elements[0].value;
        const password = e.target.elements[1].value;
        const {isUser, loginValid, passwordValid} = this.state;
        for (let i = 0; i < isUser.length; i++)
        {
            if (isUser[i].name === login && isUser[i].password === password &&
                loginValid && passwordValid) {
                window.localStorage.setItem(login, password);
                return window.location.href = '/entities';
            }
        }
        if (!loginValid || !passwordValid)
            alert('Логин должен состоять минимум из 2 символов, а пароль минимум из 6 символов');
        else 
            alert('Вы ввели неправильный логин или пароль');
    }

    validateLogin(login) {
        return login.length > 2;
    }

    validatePassword(password) {
        return password.length > 6;
    }

    onLoginChange = (e) => {
        const login = e.target.value;
        const valid = this.validateLogin(login);
        this.setState({
            login: login,
            loginValid: valid,
        }) 
    }
    
    onPasswordChange = (e) => {
        const password = e.target.value;
        const valid = this.validatePassword(password);
        this.setState({
            password: password,
            passwordValid: valid,
            
        }) 
    }

    render() {
        return (
            <div>
                <div className="container login">
                    <form onSubmit={this.handleSubmit}>
                        <ul className="login-form">
                            <li>
                                <label>Логин</label>
                                <input type='text'
                                    required
                                    value={this.state.login}
                                    onChange={this.onLoginChange}></input>
                            </li>
                            <li>
                                <label>Пароль</label>
                                <input type='text'
                                    required
                                    value={this.state.password}
                                    onChange={this.onPasswordChange}></input>
                            </li>
                            <li>
                                <button>Войти</button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        )
    }
}