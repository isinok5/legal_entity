import React, {Component} from 'react';
import Entity from './Entity';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';

export default class LegalEntities extends Component {
    state = {
        entities: [
            {
                name: "ООО Торг",
                inn: "12312312",
                address: "Проспект Медиков 3a",
                city: "Санкт-Петербург",
                phone: "777",
                director: "Иванов И.А."
            },
            {
                name: "ООО Никон",
                inn: "1232422",
                address: "Проспект мира",
                city: "Москва",
                phone: "756768",
                director: "Петров А.Е."
            },
            {
                name: "Apple",
                inn: "12343242",
                address: "Infinite Loop",
                city: "Купертино",
                phone: "7676575",
                director: "Лебедев Н.В."
            },
            {
                name: "ЗАО СПб-Автоматика",
                inn: "565534",
                address: "пр. Обуховской Обороны, д.112",
                city: "Санкт-Петербург",
                phone: "(812) 336-63-92",
                director: "Потапов Е.Д."
            },
            {
                name: "ООО В ПРАВЕ",
                inn: "7842053648",
                address: "Невский проспект, д. 173 литера А пом. 32",
                city: "Санкт-Петербург",
                phone: "(812) 367-35-32",
                director: "Внуков Ф.И."
            },
            {
                name: "ООО ИНБОКС",
                inn: "7842053648",
                address: "ул. Дрезденская, д. 26 литера А пом. 1-Н офис 102",
                city: "Санкт-Петербург",
                phone: "(812) 456-35-32",
                director: "Малеева Л.А."
            },
            {
                name: "ООО ИИС",
                inn: "7814570958",
                address: "ПРОСПЕКТ ИСПЫТАТЕЛЕЙ, Д. 30",
                city: "Санкт-Петербург",
                phone: "(812) 535-45-12",
                director: "Васильев В.А."
            },
            {
                name: "ООО Лента",
                inn: "7814148471",
                address: "ул. Савушкина, д. 112 литера Б",
                city: "Санкт-Петербург",
                phone: "(812) 3363997",
                director: "Сидоров П.А."
            },
            {
                name: "ООО ПЕРЕКРЕСТОК",
                inn: "7841005810",
                address: "ул. Караванная, д. 1",
                city: "Санкт-Петербург",
                phone: "413243631237",
                director: "Козлов Ю.В."
            },
            {
                name: "ЗАО Статус",
                inn: "7743502282",
                address: "ул.Петрозаводская, д.28, к.1",
                city: "Москва",
                phone: "44234231237",
                director: "Нестеренко И.В."
            },
        ],
        showOneEntity: false,
        showAddElem: false,
        showAddButton: true,
        buf: '',
    }
    static isLoggedIn() {
        console.log(window.localStorage);
        for (let key in window.localStorage)
        {
            if ((key === 'admin' && window.localStorage.getItem(key) === 'admin777') ||
                (key === 'user' && window.localStorage.getItem(key) === 'user777'))
                {
                    return true;
                }
        }
        return false;
    }

    componentWillMount() {
        this.addToLocal();
    }

    addToLocal() {
        let prevObj = JSON.parse(localStorage.getItem("entities"));
        if (prevObj === null)
            prevObj = [1];
        if (this.state.entities.length >= prevObj.length)
        {
            const entities = JSON.stringify(this.state.entities);
            localStorage.setItem("entities", entities);
        }
    }

    openEntity (item) {
         const {name, inn, address, city, phone, director} = item;
         const {showAddButton, showAddElem, showOneEntity} = this.state;
         this.setState({
             showOneEntity: !showOneEntity,
             showAddButton: showAddButton ? !showAddButton : showAddButton,
             showAddElem: showAddElem ? !showAddElem : showAddElem,
             buf: <Entity name={name} inn={inn} address={address} city={city} phone={phone} director={director}/>
         });
    }

    renderItems(arr) {
        if (arr) {
            return arr.map ( (item, index) => {
                const {name, inn} = item;
                return (
                    <div className="t-tr" key={index} onClick={() => this.openEntity(item)}>
                        <div className="t-td">{name}</div>
                        <div className="t-td">{inn}</div>
                    </div>
                )
            })
        }
    }

    showAddElem = () => {
        const {showAddElem, showAddButton} = this.state;
        this.setState({
            showAddButton: !showAddButton,
            showAddElem: !showAddElem,
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const obj = {
            name: e.target.elements[0].value,
            inn: e.target.elements[1].value,
            address: e.target.elements[2].value,
            city: e.target.elements[3].value,
            phone: e.target.elements[4].value,
            director: e.target.elements[5].value,
        }
        let prevObj = JSON.parse(localStorage.getItem("entities"));
        let newObj = prevObj.concat(obj);
        const entities = JSON.stringify(newObj);
        localStorage.setItem("entities", entities);
        this.setState({
            showAddElem: !this.state.showAddElem,
            showAddButton: !this.state.showAddButton,
        });
    }

    render() {
        const items = this.renderItems(JSON.parse(localStorage.getItem("entities"))); 
        const {showOneEntity, showAddButton, showAddElem} = this.state;
        return (
            <div className="container">
                {showOneEntity ?
                    this.state.buf :
                    <div className="t-lists">
                        <div className="t-tr">
                            <div className="t-td">Имя</div>
                            <div className="t-td">ИНН</div>
                        </div>
                        {items}
                    </div>
                }
                {showAddButton ? 
                    <div className="t-button" onClick={() => this.showAddElem()}>Добавить</div>
                    : null
                }
                {showAddElem ?
                    <form className="add-entity-form" onSubmit={this.handleSubmit}>
                        <li>
                            <label>Название организации</label>
                            <input type='text' required></input>
                        </li>
                        <li>
                            <label>ИНН</label>
                            <input type='text' required></input>
                        </li>
                        <li>
                            <label>Адрес</label>
                            <input type='text' required></input>
                        </li>
                        <li>
                            <label>Город</label>
                            <input type='text' required></input>
                        </li>
                        <li>
                            <label>Телефон</label>
                            <input type='text' required></input>
                        </li>
                        <li>
                            <label>Руководитель</label>
                            <input type='text' required></input>
                        </li>
                        <li>
                            <button className="t-button">OK</button>
                        </li>
                    </form> :
                    null
                }
            </div>
        )
    }
}